﻿using System;
using System.Net;
using System.Windows;
using FlickrNet;

namespace Upload2Flickr
{
    public class FlickrManager
    {
        public const string ApiKey = "1dec50e6d2f9f9c563ea13e8b1cbc60d";
        public const string SharedSecret = "e960782c413f30c0";

        public static Flickr GetInstance()
        {
            return new Flickr(ApiKey, SharedSecret);
        }

        public static Flickr GetAuthInstance()
        {
            var f = new Flickr(ApiKey, SharedSecret);
            f.OAuthAccessToken = OAuthToken.Token;
            f.OAuthAccessTokenSecret = OAuthToken.TokenSecret;
            return f;
        }

        public static OAuthAccessToken OAuthToken
        {
            get
            {
                return Properties.Settings.Default.OAuthToken;
            }
            set
            {
                Properties.Settings.Default.OAuthToken = value;
                Properties.Settings.Default.Save();
            }
        }

    }
}
