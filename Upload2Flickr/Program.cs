﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Upload2Flickr
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            string Folder = null;
            bool IsPublic = false;
            if (args.Length > 1)
            {
                bool error = false;
                if (args.Length < 3)
                {
                    Folder = args[0];
                    if (args.Length == 2)
                    {
                        if (args[1] == "/public")
                            IsPublic = true;
                        else
                            error = true;
                    }
                }
                else
                {
                    error = true;
                }
                if (error)
                {
                    Folder = null;
                    MessageBox.Show("Usage: Upload2Flickr TargetFolder [/public]");
                }
            }
            Application.Run(new Main(Folder, IsPublic));
        }
    }
}
