﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Upload2Flickr
{
    public partial class Main : Form
    {
        string Folder = null;
        bool IsPublic = false;

        public Main(string folder = null, bool isPublic = false)
        {
            InitializeComponent();
            Folder = folder;
            IsPublic = isPublic;
        }

        private void GoButton_Click(object sender, EventArgs e)
        {
            DialogResult res = System.Windows.Forms.DialogResult.OK;
            while ((FlickrManager.OAuthToken == null || FlickrManager.OAuthToken.Token == null) && res != DialogResult.Cancel)
            {
                AuthForm f = new AuthForm();
                res = f.ShowDialog();
            }
            RefreshUserLabel();
            if (!(FlickrManager.OAuthToken == null || FlickrManager.OAuthToken.Token == null))
            {
                RefreshButtons(false);
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            string path;
            try
            {
                var fl = FlickrManager.GetAuthInstance();
                
                string[] files = Directory.GetFiles(folderTextBox.Text, "*.*", SearchOption.AllDirectories);
                
                for (int i = 0; i < files.Length; i++)
                {
                    path = files[i].Replace("..", ".");

                    if (e.Cancel) return;
                    if (Path.GetFileNameWithoutExtension(path).StartsWith(".")) continue;

                    try
                    {
                        string photoId = fl.UploadPicture(files[i], Path.GetFileNameWithoutExtension(path), null, Path.GetDirectoryName(path).Replace("C:\\_Fotos\\", "").Replace("\\", " "), publicCheckBox.Checked, false, false);
                    }
                    catch
                    {
                        Log(files[i]);
                    }
                    
                    backgroundWorker1.ReportProgress((int)(((double)i / (double)files.Length) * 100));
                }
            }
            catch (Exception exc)
            {
                Log(exc.Message);
            }
        }

        private void Log(string error)
        {
            using (var log = File.AppendText("log.txt"))
            {
                log.WriteLine(error);
                log.Flush();
                log.Close();
            }
        }

        private bool CreatePath(string path, bool file)
        {
            DirectoryInfo di = null;
            try
            {
                if (file)
                {
                    FileInfo fi = new FileInfo(path);
                    di = fi.Directory;
                }
                else
                {
                    di = new DirectoryInfo(path);
                }
                if (!di.Exists)
                {
                    DirectoryInfo p = di.Parent;
                    if (CreatePath(p.FullName, false))
                    {
                        Directory.CreateDirectory(di.FullName);
                        //p.CreateSubdirectory(di.FullName);
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                string dir = path;
                if (di != null)
                    dir = di.FullName;
                MessageBox.Show("Creating folder " + dir + " failed");
                return false;
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            RefreshButtons(true);

            if (Folder == null)
            {
                MessageBox.Show("Upload completed");
            }
            else
            {
                this.Close();
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.LastFolder == null)
            {
                folderTextBox.Text = System.Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            }
            else
            {
                folderTextBox.Text = Properties.Settings.Default.LastFolder;
            }
            RefreshUserLabel();
            if (Folder != null)
            {
                folderTextBox.Text = Folder;
                publicCheckBox.Checked = IsPublic;
                GoButton_Click(null, null);
            }
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            RefreshButtons(true);

            backgroundWorker1.CancelAsync();

            Folder = null;

            MessageBox.Show("Upload stopped");
        }

        private void RefreshButtons(bool enabled)
        {
            GoButton.Enabled = enabled;
            publicCheckBox.Enabled = enabled;
            StopButton.Enabled = !enabled;
            SetUserButton.Enabled = enabled;
            folderTextBox.Enabled = enabled;
            FolderButton.Enabled = enabled;
        }

        private void FolderButton_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = folderTextBox.Text;
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                folderTextBox.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void folderTextBox_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.LastFolder = folderTextBox.Text;
            Properties.Settings.Default.Save();
        }

        private void SetUserButton_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AuthForm f = new AuthForm();
            f.ShowDialog();
            RefreshUserLabel();
        }

        private void RefreshUserLabel()
        {
            if (FlickrManager.OAuthToken == null || FlickrManager.OAuthToken.Token == null)
            {
                SetUserButton.Text = "Not logged in";
            }
            else
            {
                SetUserButton.Text = "Logged in as " + FlickrManager.OAuthToken.FullName;
            }
        }
    }
}
